package com.glandore.oliverplunkett.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.glandore.oliverplunkett.R;
import com.glandore.oliverplunkett.STSApp;
import com.glandore.oliverplunkett.Utils.Constant;
import com.glandore.oliverplunkett.Utils.ParseRss;
import com.glandore.oliverplunkett.data.Event;

import java.util.ArrayList;
import java.util.Collections;

public class StartupActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_startup);
        if (isNetworkConnected()){
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (STSApp.arrNews == null){
                        STSApp.arrNews = reloadListNewses();
                    }
                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    startActivity(intent);
                    finish();
                }
            }).start();
        } else {
            TextView msg = (TextView) findViewById(R.id.txt_msg_err_net);
            msg.setVisibility(View.VISIBLE);
        }
    }

    private ArrayList<Event> reloadListNewses() {
        ParseRss rss = new ParseRss(Constant.URL_RSS);
        rss.fetchXML();

        while(rss.parsingComplete);

        Collections.sort(rss.getEvents());
        return rss.getEvents();
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    public void tryAgain(View view) {
        TextView msg = (TextView) findViewById(R.id.txt_msg_err_net);
        msg.setVisibility(View.GONE);
        if (isNetworkConnected()){
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (STSApp.arrNews == null){
                        STSApp.arrNews = reloadListNewses();
                    }
                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    startActivity(intent);
                    finish();
                }
            }).start();
        } else {
            msg.setVisibility(View.VISIBLE);
        }
    }
}
