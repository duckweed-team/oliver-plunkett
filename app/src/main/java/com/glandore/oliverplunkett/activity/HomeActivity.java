package com.glandore.oliverplunkett.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.LevelListDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.glandore.oliverplunkett.R;
import com.glandore.oliverplunkett.STSApp;
import com.glandore.oliverplunkett.Utils.Constant;
import com.glandore.oliverplunkett.data.Event;
import com.glandore.oliverplunkett.ui.EventAdapter;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import org.w3c.dom.Text;

import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;

public class HomeActivity extends AppCompatActivity implements OnMapReadyCallback{

    FrameLayout frameLayout = null;
    FrameLayout frameMain = null;
    View contentHome = null;
    View contentDetail = null;
    View contentLocation = null;
    View contentFood = null;
    View contentAbout = null;
    View contentShareMenu = null;

    ArrayList<Event> events = null;
    int curTab = R.id.nav_home;
    int typeHome = 1;

    // Cua tao code nghe
    private CallbackManager callbackManager;
    private LoginManager loginManager;

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        RadioButton home = (RadioButton)findViewById(R.id.nav_home);
        if (home != null) {
            home.setOnCheckedChangeListener(btnNavBarOnCheckedChangeListener);
        }
        RadioButton food = (RadioButton)findViewById(R.id.nav_food);
        if (food != null) {
            food.setOnCheckedChangeListener(btnNavBarOnCheckedChangeListener);
        }
        RadioButton location = (RadioButton)findViewById(R.id.nav_location);
        if (location != null) {
            location.setOnCheckedChangeListener(btnNavBarOnCheckedChangeListener);
        }
        RadioButton about = (RadioButton)findViewById(R.id.nav_about);
        if (about != null) {
            about.setOnCheckedChangeListener(btnNavBarOnCheckedChangeListener);
        }

        frameLayout = (FrameLayout) findViewById(R.id.frameBody);
        frameMain = (FrameLayout) findViewById(R.id.frameMain);
        LayoutInflater inflater = (LayoutInflater)getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);
        contentHome = inflater.inflate(R.layout.content_home, null);
        contentDetail = inflater.inflate(R.layout.content_detail, null);
        contentFood = inflater.inflate(R.layout.content_food, null);
        contentLocation = inflater.inflate(R.layout.content_location, null);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        contentAbout = inflater.inflate(R.layout.content_about, null);
        contentShareMenu = inflater.inflate(R.layout.content_share_menu, null);

        if (frameLayout != null) {
            frameLayout.addView(contentHome);
        }

        events = STSApp.arrNews;

        EventAdapter adapter = new EventAdapter(this, R.layout.event_item, events);

        ListView listNews = (ListView)contentHome.findViewById(R.id.list_item);
        if (listNews != null){
            listNews.setAdapter(adapter);
            for (Event n : events)
            {
                n.loadImage(adapter);
            }
            listNews.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    typeHome = 2;
                    frameLayout.removeAllViews();
                    frameLayout.addView(contentDetail);
                    STSApp.curNews = events.get(position);
                    setContentEvent(events.get(position));
                }
            });
        }
    }

    private void setContentEvent(Event event)
    {
        WebView youtube = (WebView) contentDetail.findViewById(R.id.youtube_view);
        TextView title = (TextView) contentDetail.findViewById(R.id.title_item);
        TextView info = (TextView) contentDetail.findViewById(R.id.info_item);
        TextView txtDay = (TextView) contentDetail.findViewById(R.id.calendar_day);
        TextView txtMonth = (TextView) contentDetail.findViewById(R.id.calendar_month);
        TextView txtHour = (TextView) contentDetail.findViewById(R.id.txtHour);
        TextView txtAdmission = (TextView) contentDetail.findViewById(R.id.admission_item);
        Button btnBuyTickets = (Button) contentDetail.findViewById(R.id.buy_tickets);
        youtube.getSettings().setJavaScriptEnabled(true);
        youtube.loadUrl(event.getYoutube());
        title.setText(event.getName());
        title.setTypeface(Typeface.createFromAsset(getAssets(),"Quicksand-Bold.otf"));
        info.setText(Html.fromHtml(event.getInfo()));
        SimpleDateFormat timeFormat = new SimpleDateFormat("dd");
        txtDay.setText(timeFormat.format(event.getDateObj()));
        timeFormat = new SimpleDateFormat("MMM");
        txtMonth.setText(timeFormat.format(event.getDateObj()));
        txtHour.setText(event.getTime());
        if(event.getAdmission() != null && event.getAdmission().equals("FREE")){
            txtAdmission.setVisibility(View.VISIBLE);
            btnBuyTickets.setVisibility(View.GONE);
        } else {
            txtAdmission.setVisibility(View.GONE);
            btnBuyTickets.setVisibility(View.VISIBLE);
        }
    }

    private CompoundButton.OnCheckedChangeListener btnNavBarOnCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                frameLayout.removeAllViews();
                if (curTab == R.id.nav_home && typeHome == 2) {
                    WebView webview = (WebView) contentDetail.findViewById(R.id.youtube_view);
                    webview.loadUrl(STSApp.curNews.getYoutube());
                }
                curTab = buttonView.getId();
                switch (curTab){
                    case R.id.nav_home:
                        if (typeHome == 1)
                            frameLayout.addView(contentHome);
                        else
                            frameLayout.addView(contentDetail);
                        break;
                    case R.id.nav_location:
                        frameLayout.addView(contentLocation);
                        break;
                    case R.id.nav_food:
                        frameLayout.addView(contentFood);
                        break;
                    case R.id.nav_about:
                        frameLayout.addView(contentAbout);
                        break;
                }
            }
        }
    };

    public void showMenu(View v) {
        frameMain.addView(contentShareMenu);
    }

    public void hideMenu(View view) {
        frameMain.removeView(contentShareMenu);
    }

    public void backHome(View view) {
        typeHome = 1;
        WebView webview = (WebView) contentDetail.findViewById(R.id.youtube_view);
        webview.loadUrl("");
        frameLayout.removeAllViews();
        frameLayout.addView(contentHome);
    }

    public void shareViaFacebook(View view)
    {
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        List<String> permissionNeeds = new ArrayList<>();

        loginManager = LoginManager.getInstance();
        loginManager.logInWithPublishPermissions(this, permissionNeeds);

        final ShareDialog shareDialog = new ShareDialog(this);

        if (ShareDialog.canShow(ShareLinkContent.class))
        {
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setContentTitle(STSApp.curNews.getName())
                    .setContentDescription(Html.fromHtml(STSApp.curNews.getInfo()).toString())
                    .setImageUrl(Uri.parse(STSApp.curNews.getImg()))
                    .setContentUrl(Uri.parse(STSApp.curNews.getWeb()))
                    .build();

            shareDialog.show(linkContent);
        }
    }

    public void shareViaTwitter(View view)
    {
        TwitterAuthConfig authConfig = new TwitterAuthConfig(Constant.TWITTER_CONSUMER_KEY, Constant.TWITTER_CONSUMER_SECRET);
        Fabric.with(this, new TwitterCore(authConfig), new TweetComposer());

        TweetComposer.Builder builder = new TweetComposer.Builder(this)
                .text(STSApp.curNews.getName())
                .image(Uri.parse(STSApp.curNews.getImg()));
        builder.show();
    }

    public void shareViaEmail(View view)
    {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", "", null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, STSApp.curNews.getName());
        emailIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(STSApp.curNews.getInfo()).toString());
        startActivity(Intent.createChooser(emailIntent, "Send email..."));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney, Australia, and move the camera.
        LatLng oliver = new LatLng(Constant.LATITUDE, Constant.LONGITUDE);
        mMap.addMarker(new MarkerOptions()
                .position(oliver)
                .icon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons(Constant.ICON_WIDTH, Constant.ICON_HEIGHT))));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(oliver, Constant.ZOOM));

        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
    }

    public Bitmap resizeMapIcons(int width, int height){
        Bitmap image = BitmapFactory.decodeResource(getResources(), R.drawable.maps_marker);
        Bitmap resizedBitmap = Bitmap.createScaledBitmap(image, width, height, false);
        return resizedBitmap;
    }

    public void buyTickets(View view) {
        String uri = STSApp.curNews.getTicketLink();
        if (!uri.startsWith("http://") && !uri.startsWith("https://")){
            uri = "http://"+uri;
        }
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        startActivity(browserIntent);
    }

    public void clickHome(View view) {
        RadioButton navBtn = (RadioButton) findViewById(R.id.nav_home);
        if (navBtn!=null) {
            navBtn.setChecked(true);
        }
    }

    public void clickLocation(View view) {
        RadioButton navBtn = (RadioButton) findViewById(R.id.nav_location);
        if (navBtn!=null) {
            navBtn.setChecked(true);
        }
    }

    public void clickFood(View view) {
        RadioButton navBtn = (RadioButton) findViewById(R.id.nav_food);
        if (navBtn!=null) {
            navBtn.setChecked(true);
        }
    }

    public void clickAbout(View view) {
        RadioButton navBtn = (RadioButton) findViewById(R.id.nav_about);
        if (navBtn!=null) {
            navBtn.setChecked(true);
        }
    }
}
