package com.glandore.oliverplunkett.Utils;

import android.net.ParseException;

import com.glandore.oliverplunkett.data.Event;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by hop on 03/06/2016.
 */
public class ParseRss
{
    private ArrayList<Event> events = new ArrayList<Event>();
    private String urlString = null;
    private XmlPullParserFactory xmlFactoryObject;
    public volatile boolean parsingComplete = true;

    public static final int READ_TIME_OUT = 10000;
    public static final int CONNECT_TIME_OUT = 15000;

    public ParseRss(String url)
    {
        this.urlString = url;
    }

    public ArrayList<Event> getEvents()
    {
        return events;
    }

    public void parseXMLAndStoreIt(XmlPullParser myParser)
    {
        int event;
        String text = null;
        try
        {
            event = myParser.getEventType();
            Event item = new Event();

            while (event != XmlPullParser.END_DOCUMENT)
            {
                String name = myParser.getName();

                switch (event)
                {
                    case XmlPullParser.START_TAG:
                        break;
                    case XmlPullParser.TEXT:
                        text = myParser.getText();
                        break;
                    case XmlPullParser.END_TAG:
                        if(name.equals("event"))
                        {
                            events.add(item);
                            item = new Event();
                        }
                        else if (name.equals("eventID"))
                        {
                            item.setEventId(text);
                        }
                        else if (name.equals("name"))
                        {
                            item.setName(text);
                        }
                        else if (name.equals("venue"))
                        {
                            item.setVenue(text);
                        }
                        else if (name.equals("date"))
                        {
                            item.setDate(text);
                            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                            try {
                                item.setDateObj(format.parse(text));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                        }
                        else if (name.equals("time"))
                        {
                            item.setTime(text);
                        }
                        else if (name.equals("category"))
                        {
                            item.setCategory(text);
                        }
                        else if (name.equals("admission"))
                        {
                            item.setAdmission(text);
                        }
                        else if (name.equals("info"))
                        {
                            item.setInfo(text);
                        }
                        else if (name.equals("img"))
                        {
                            item.setImg(text);
                        }
                        else if (name.equals("youtube"))
                        {
                            item.setYoutube(text);
                        }
                        else if (name.equals("web"))
                        {
                            item.setWeb(text);
                        }
                        else if (name.equals("gaeilge"))
                        {
                            item.setGaeilge(text);
                        }
                        else if (name.equals("ticket_link"))
                        {
                            item.setTicketLink(text);
                        }
                        else
                        {

                        }
                        break;
                }

                event = myParser.next();
            }

            parsingComplete = false;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void fetchXML()
    {
        Thread thread = new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    URL url = new URL(urlString);
                    HttpURLConnection conn = (HttpURLConnection)url.openConnection();

                    conn.setReadTimeout(READ_TIME_OUT);
                    conn.setConnectTimeout(CONNECT_TIME_OUT);
                    conn.setRequestMethod("GET");
                    conn.setDoInput(true);

                    conn.connect();
                    InputStream stream = conn.getInputStream();

                    xmlFactoryObject = XmlPullParserFactory.newInstance();
                    XmlPullParser mParser = xmlFactoryObject.newPullParser();

                    mParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                    mParser.setInput(stream, null);

                    parseXMLAndStoreIt(mParser);
                    stream.close();
                }
                catch (Exception e)
                {

                }
            }
        });
        thread.start();
    }
}
