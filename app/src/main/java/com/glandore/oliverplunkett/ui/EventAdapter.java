package com.glandore.oliverplunkett.ui;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.glandore.oliverplunkett.R;
import com.glandore.oliverplunkett.data.Event;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by TAKITORI on 02/06/2016.
 */
public class EventAdapter extends ArrayAdapter<Event> {
    Context context;
    int layoutResource;
    ArrayList<Event> events = null;

    public EventAdapter(Context context, int layoutResource, ArrayList<Event> events){
        super(context, layoutResource, events);

        this.context = context;
        this.layoutResource = layoutResource;
        this.events = events;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null)
        {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            convertView = inflater.inflate(layoutResource, parent, false);

            holder = new ViewHolder();
            holder.imgThumb = (ImageView) convertView.findViewById(R.id.imgThumb);
            holder.txtDay = (TextView) convertView.findViewById(R.id.calendar_day);
            holder.txtMonth = (TextView) convertView.findViewById(R.id.calendar_month);
            holder.txtHour = (TextView) convertView.findViewById(R.id.txtHour);
            holder.txtTitle = (TextView) convertView.findViewById(R.id.txtTitle);
            holder.txtAdmission = (TextView) convertView.findViewById(R.id.admission_item);
            holder.btnBuyTickets = (Button) convertView.findViewById(R.id.buy_tickets);

            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder)convertView.getTag();
        }

        Event item = events.get(position);
        if (item == null){
            return null;
        }

        if (item.getImg() != null && holder.imgThumb != null)
        {
            holder.imgThumb.setImageBitmap(item.getImgThumb());
        }

        if (item.getDateObj() != null){
            SimpleDateFormat timeFormat = new SimpleDateFormat("dd");
            holder.txtDay.setText(timeFormat.format(item.getDateObj()));
            timeFormat = new SimpleDateFormat("MMM");
            holder.txtMonth.setText(timeFormat.format(item.getDateObj()));
        }

        if (item.getTime() != null){
            holder.txtHour.setText(item.getTime());
        }

        if(!item.getName().equals("")){
            holder.txtTitle.setText(item.getName());
            holder.txtTitle.setTypeface(Typeface.createFromAsset(context.getAssets(),"Quicksand-Bold.otf"));
        }

        if(item.getAdmission() != null && holder.txtAdmission!= null){
            holder.txtAdmission.setText("Admission: " + Html.fromHtml(item.getAdmission()));
        }

        return convertView;
    }

    static class ViewHolder {
        ImageView imgThumb;
        TextView txtDay;
        TextView txtMonth;
        TextView txtHour;
        TextView txtAdmission;
        Button btnBuyTickets;
        TextView txtTitle;
    }
}
