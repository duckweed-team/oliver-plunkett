package com.glandore.oliverplunkett.data;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import com.glandore.oliverplunkett.ui.EventAdapter;

import java.io.InputStream;
import java.util.Date;

/**
 * Created by TAKITORI on 02/06/2016.
 */
public class Event implements Comparable<Event>
{
    private Bitmap imgThumb;
    private String eventId;
    private String name;
    private String venue;
    private String date;
    private Date dateObj;
    private String time;
    private String category;
    private String admission;
    private String info;
    private String img;
    private String youtube;
    private String web;
    private String gaeilge;
    private String ticketLink;

    private EventAdapter eventAdapter;

    public Event()
    {

    }

    public Bitmap getImgThumb() {
        return imgThumb;
    }

    public String getEventId() {
        return eventId;
    }

    public String getName() {
        return name;
    }

    public String getVenue() {
        return venue;
    }

    public String getDate() {
        return date;
    }

    public Date getDateObj() {
        return dateObj;
    }

    public String getTime() {
        return time;
    }

    public String getCategory() {
        return category;
    }

    public String getAdmission() {
        return admission;
    }

    public String getInfo() {
        return info;
    }

    public String getImg() {
        return img;
    }

    public String getYoutube() {
        return youtube;
    }

    public String getWeb() {
        return web;
    }

    public String getGaeilge() {
        return gaeilge;
    }

    public String getTicketLink() {
        return ticketLink;
    }

    public void setImgThumb(Bitmap imgThumb) {
        this.imgThumb = imgThumb;
    }

    public EventAdapter getEventAdapter() {
        return eventAdapter;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setDateObj(Date dateObj) {
        this.dateObj = dateObj;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setAdmission(String admission) {
        this.admission = admission;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public void setYoutube(String youtube) {
        this.youtube = youtube;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public void setGaeilge(String gaeilge) {
        this.gaeilge = gaeilge;
    }

    public void setTicketLink(String ticketLink) {
        this.ticketLink = ticketLink;
    }

    public void setEventAdapter(EventAdapter eventAdapter) {
        this.eventAdapter = eventAdapter;
    }

    public void loadImage(EventAdapter sta) {
        // HOLD A REFERENCE TO THE ADAPTER
        this.eventAdapter = sta;
        if (img != null && !img.equals("")) {
            new ImageLoadTask().execute(img);
        }
    }

    @Override
    public int compareTo(Event another) {
        if (getDateObj() == null || another.getDateObj() == null)
            return 0;

        int value = getDateObj().compareTo(another.getDateObj());

        return value;
    }

    // ASYNC TASK TO AVOID CHOKING UP UI THREAD
    private class ImageLoadTask extends AsyncTask<String, String, Bitmap> {

        @Override
        protected void onPreExecute() {
            Log.i("ImageLoadTask", "Loading image...");
        }

        // PARAM[0] IS IMG URL
        protected Bitmap doInBackground(String... param) {
            Log.i("ImageLoadTask", "Attempting to load image URL: " + param[0]);
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(param[0]).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onProgressUpdate(String... progress) {
            // NO OP
        }

        protected void onPostExecute(Bitmap ret) {
            if (ret != null) {
                Log.i("ImageLoadTask", "Successfully loaded " + name + " image");
                imgThumb = ret;
                if (eventAdapter != null) {
                    // WHEN IMAGE IS LOADED NOTIFY THE ADAPTER
                    eventAdapter.notifyDataSetChanged();
                }
            } else {
                Log.e("ImageLoadTask", "Failed to load " + name + " image");
            }
        }
    }
}
